from django.shortcuts import render, redirect 
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .models import Feedback, Relawan
from .forms import FeedbackForm, RelawanForm
from .models import Feedback,  news
from .forms import FeedbackForm, berita
import datetime
from pip._vendor import requests
import json

from django.contrib.auth.decorators import login_required

# Create your views here.
from .models import CreateUserForm
from django.core import serializers
@csrf_exempt

def index(request):
    return render(request, 'index.html')

@login_required(login_url='/login/')
def form(request):
    myForm = RelawanForm()

    if request.method == 'POST':
        myForm = RelawanForm(request.POST)
        if myForm.is_valid():
            Relawan.objects.create(**myForm.cleaned_data)
            return redirect("TugasKelompokPPWApp:thanks")

    context = {
        'form' : myForm,
    }
    return render(request, 'form.html', context)

def afterForm(request):
    return render(request, "afterform1.html")

def register(request):
    if request.user.is_authenticated:
        return redirect('TugasKelompokPPWApp:home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('TugasKelompokPPWApp:login')

        context = {'form' :form}
        return render(request, 'register.html', context)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('TugasKelompokPPWApp:home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('TugasKelompokPPWApp:home')
            else:
                messages.info(request, 'username or password is incorrect')

        context = {}
        return render(request, 'login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('TugasKelompokPPWApp:home')

@login_required(login_url='/login/')
def feedback_index(request, *args, **kwargs):
    my_form = FeedbackForm(Feedback)
    feedbacks =Feedback.objects.all()

    context = {
        'form' : my_form,
        'feedbacks' : feedbacks
    }
    return render(request, "feedback.html", context)  

@login_required(login_url='/login/')
# def feedback_create(request):
#     my_form = FeedbackForm()

#     if request.method == 'POST':
#         my_form = FeedbackForm(request.POST)
#         if my_form.is_valid():
#             Feedback.objects.create(**my_form.cleaned_data)
#             return redirect("TugasKelompokPPWApp:feedback")

#     context = {
#         'form' : my_form,
#     }
#     return render(request, "feedback_create.html", context)  


@login_required(login_url='/login/')
def beritas(request):
    form = berita()
    context = {'form':form}
    if request.method == "POST":
        form = berita(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/hasil')
    return render(request, 'berita.html', context)

@login_required(login_url='/login/')
def hasil(request):
    result = news.objects.all()
    context = {'result':result}
    return render(request, 'hasil.html', context)

@login_required(login_url='/login/')
def data(request):
    url = request.GET['q']
    ret = serializers.serialize('json', news.objects.filter(title__icontains=url))

    data = json.loads(ret)
    return JsonResponse(data,safe=False)

# #def detail_post(request, id):
#     thepost = Feedback.objects.get(id=id)
#     likes = len(Like.objects.filter(post = thepost))
#     context = {
#         "post" : thepost,
#         "like" : likes,
#     }
#     return render(request, "feedback.html", context)

# def update_like(request, id):
#     post_like = len(Like.objects.filter(post=Feedback.objects.get(id=id)))
#     return JsonResponse(post_like, safe=False)


def feedback_create(request):
    my_form = FeedbackForm()
    response = {}
    if request.method == 'POST':
        # print("APA AJA")
        if request.is_ajax():
            my_form = FeedbackForm(request.POST)
            print("aaaaaaaaaaaaaaaaaa")
            
            if my_form.is_valid():
                print("zzzzzzzzzzzzzzzzzzzzzzzz")
                Feedback.objects.create(**my_form.cleaned_data)
                return JsonResponse({'success':True})
        return JsonResponse({'success':True})

    context = {
        'form' : my_form,
    }
    print("bbbbbbbbb")
    return render(request, "feedback_create.html", context)  


def getTipsTrick(request):
    userReq = Feedback.objects.all().values()
    lst = list(userReq)
    print(lst)
    return JsonResponse(lst, safe=False)
