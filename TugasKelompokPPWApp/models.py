from django.db import models
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


# Create your models here.
class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
        
class Feedback(models.Model) :
    nama = models.TextField(max_length=20)
    pesan = models.TextField(max_length=1200)

    def get_dict(self):
        return {
            'nama':self.nama,
            'pesan':self.pesan
        }
    
    def __str__(self):
        return self.nama



class Relawan(models.Model):
    nama = models.TextField(max_length = 100)
    email = models.TextField(max_length = 30)
    alasan = models.TextField(max_length = 300)

class news(models.Model):
    title = models.CharField(max_length=50)
    author = models.CharField(max_length=50)
    link = models.CharField(max_length=500)
    content = models.TextField()

