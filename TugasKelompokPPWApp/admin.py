from django.contrib import admin

# Register your models here.
from .models import Feedback, news, Relawan

# Register your models here.

admin.site.register(Feedback)
admin.site.register(Relawan)
@admin.register(news)
class admin_news(admin.ModelAdmin):
    list_display = ('title', 'author', 'link', 'content')
