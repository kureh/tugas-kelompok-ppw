from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Feedback, news
from .views import index ,form,feedback_create,feedback_index, beritas, hasil
from .forms import * 
from .apps import AppConfig
from django.http import HttpRequest
from .urls import urlpatterns
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, authenticate, get_user


# Create your tests here.
class UnitTestForTugasKelompokPPWApp(TestCase):
    def test_index_routing_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_home_routing_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_form_routing_exist(self):
        response = Client().get('/form/')
        self.assertEqual(response.status_code, 302)
    
    def test_feedback_routing_exist(self):
        response = Client().get('/feedback/')
        self.assertEqual(response.status_code, 302)

    def test_save_a_POST_request_2(self):
        response = Client().post('/feedback-create/', data={'nama':'PPW', 'pesan':'mengerjakan story'})
        self.assertEqual(response.status_code,200)


    def test_check_function_used_by_landingpage(self):
        found = resolve('/')
        self.assertEqual(found.func, index)


    def test_index_routing_correct(self):
        function = resolve('/')
        self.assertNotEqual(function.func,feedback_create)

    def test_index_html_render(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_navbar_html_render(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'navbar.html')

    def test_FeedbackForm_valid(self):
        form = FeedbackForm(data={'nama': "zahra", 'pesan': "sangat menarik"})
        self.assertTrue(form.is_valid())
    
    def test_FeedbackForm_invalid(self):
        form = FeedbackForm(data={'nama': "", 'pesan': "mp"})
        self.assertFalse(form.is_valid())

    def test_creating_an_object(self):
        status = Feedback.objects.create(nama='Test', pesan='We are testing')
        self.assertEqual(Feedback.objects.count(), 1)
    
    def test_save_a_POST_request(self):
        response = Client().post('/', data={'nama':'PPW', 'pesan':'mengerjakan story'})
        self.assertEqual(response.status_code,200)
        self.assertEqual(response.status_code,200)

class Berita_Test(TestCase):
    
#Isi Berita
    def test_url_berita(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 302)

    def test_berita_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, beritas)
    

#Daftar Hasil
    def test_url_daftarkegiatan(self):
        response = Client().get('/hasil/')
        self.assertEqual(response.status_code, 302)


    def test_daftarkegiatan_func(self):
        found = resolve('/hasil/')
        self.assertEqual(found.func, hasil)
    
    def test_view_dalam_template_daftarkegiatan(self):
        response = Client().get('/hasil/')  
        isi_view = response.content.decode('utf8')

#Search Berita
    def test_search_berita(self):
        news.objects.create(title="judul", author="penulis", content="konten", link="link")
        response = Client().get("/hasil/data/?q=")
        new = news.objects.all()
        self.assertEqual(response.status_code,302)

class AuthTest(TestCase):

    # TODO: Test for django messages, test login errors

    def test_register_password_didnt_match(self):
        register_data = {
            "username": "test_dummy_different_pass",
            "password1": "abc5dasar",
            "password2": "abc5dodol"
        }
        
        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "two password fields didn’t match")

    def test_register_username_taken(self):
        register_data = {
            "username": "test_dummy_duplicate",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 302)
        
        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "user with that username already exists")

    def test_register_password_too_common(self):
        register_data = {
            "username": "test_dummy_too_common",
            "password1": "asdf12345",
            "password2": "asdf12345"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "password is too common")
        
    def test_success_register_login_logout(self):
        register_data = {
            "username": "test_dummy",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

        response = self.client.post('/register/', data=register_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/login/')

        # Login user
        login_data = {
            "username": "test_dummy",
            "password": "abc5dasar"
        }
        response = self.client.post('/login/', data=login_data)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/home/')
        
        # Check auth status after login
        user = get_user(self.client)
        self.assertTrue(user.is_authenticated)

        # Logout user
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/home/')
        
        # Check auth status after logout
        user = get_user(self.client)
        self.assertFalse(user.is_authenticated)

    
