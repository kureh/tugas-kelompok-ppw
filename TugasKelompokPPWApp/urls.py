from django.urls import path
from django.contrib import admin
from .views import feedback_index, feedback_create
from . import views

app_name = 'TugasKelompokPPWApp'

urlpatterns = [
    path('', views.index, name='index'),
    path('home/', views.index, name='home'),
    path('form/', views.form, name='form'),
    path('form/afterform1.html/', views.afterForm, name='thanks'),
    path('register/', views.register, name='register'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('feedback/', feedback_index, name='feedback'),
    path('feedback-create/', feedback_create, name="feedbackCreate"),
    path('berita/', views.beritas, name='beritas'),
    path('hasil/', views.hasil, name='hasil'),
    path('hasil/data/', views.data,  name='data'),

    #path('updateLike/<int:id>',views.update_like,name='update_like'),
    path('getTipsTrick/', views.getTipsTrick, name='getTipsTrick')
]