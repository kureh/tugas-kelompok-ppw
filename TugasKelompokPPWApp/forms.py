from django import forms
from django.forms import ModelForm
from .models import news



class FeedbackForm(forms.Form):
    nama = forms.CharField(label="Nama", widget=forms.Textarea(attrs={
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Masukkan Nama Anda',
        'required': True,
        'id' : 'nama_input'
    }))

    pesan = forms.CharField(label="Tips & Trick", widget=forms.Textarea(attrs={
        'rows': 6,
        'style': 'resize:none;',
        'class': 'form-control',
        'type' : 'text',
        'placeholder': 'Tulis feedback versi kamu!',
        'required': True,
        'id':'message_input'
    }))


class RelawanForm(forms.Form) :
    nama = forms.CharField(label="Nama", max_length = 25)
    email = forms.CharField(label="Email", max_length = 30)
    alasan = forms.CharField(label="Alasan", max_length = 300)

class berita(ModelForm):
    class Meta:
        model = news
        fields = '__all__'

