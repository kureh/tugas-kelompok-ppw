from django.apps import AppConfig


class TugaskelompokppwappConfig(AppConfig):
    name = 'TugasKelompokPPWApp'
