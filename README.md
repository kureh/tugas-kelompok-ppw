1. Nama-Nama Anggota Kelompok
- Ulul 'Azmi Abdullah Iman
- Cut Zahra Nabila Fahmi
- Muhammad Ivan Radman
- Haidar Labib Bangsawijaya

2. Link Herokuapp
https://maungcegahcovid.herokuapp.com/

3. Cerita aplikasi yang diajukan serta kebermanfaatannya
Aplikasi kami dibuat untuk bisa mengedukasikan masyarakat mengenai covid-19 dan cara mencegahnya, serta dapat melihat live count dari berapa banyak masyarakat yang sudah positif dan meninggal agar bisa membuat masyarakat lebih waspada. Website kita juga memiliki form untuk orang-orang yang ingin mendaftar untuk menjadi relawan untuk mengedukasi masyarakat. 

4. Daftar fitur yang akan diimplementasikan
- Live count, bisa melihat berapa banyak kasus yang terjadi di daerah depok
- Navbar, bisa mengganti-ganti page tinggal ngeklik tombol di Navbar
- Form, bisa mengisi form untuk mendaftar jadi relawan mengedukasi covid-19
- Home Page, berisikan landing page dari website 